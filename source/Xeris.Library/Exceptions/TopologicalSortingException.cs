﻿using System;
using System.Collections.Generic;
using Xeris.Language;

namespace Xeris.Library.Exceptions
{
    public class TopologicalSortingException<T> : Exception, IDisposable
    {
        string message;
        T node;
        List<T> cycleList;

        public TopologicalSortingException(T node, List<T> cycleList)
        {
            this.message = LanguageFile.Exception_TopologicalSortingException;
            this.node = node;
            this.cycleList = cycleList;
        }

        public void Dispose()
        {
            this.message = null;
            this.node = default(T);
            this.cycleList = null;
        }

        public override string Message
        {
            get
            {
                return message;
            }
        }

        public T Node
        {
            get
            {
                return node;
            }
        }

        public List<T> CycleList
        {
            get
            {
                return cycleList;
            }
        }
    }
}

﻿using System;
using System.Runtime.InteropServices;
using Xeris.Language;
using Xeris.Library.Logging;

namespace Xeris.Library.Utilities
{
    public static class Time
    {
        public static bool IsAvailable { get; private set; }
        public static DateTime Unix { get; private set; }

        [DllImport("Kernel32.dll", CallingConvention = CallingConvention.Winapi)]
        private static extern void GetSystemTimePreciseAsFileTime(out long filetime);

        public static DateTime UtcNow
        {
            get
            {
                if (!IsAvailable)
                    return DateTime.UtcNow;

                long filetime;
                GetSystemTimePreciseAsFileTime(out filetime);
                return DateTime.FromFileTimeUtc(filetime);
            }
        }

        public static DateTime Now
        {
            get
            {
                if (!IsAvailable)
                    return DateTime.Now;

                long filetime;
                GetSystemTimePreciseAsFileTime(out filetime);
                return DateTime.FromFileTime(filetime);
            }
        }

        public static long UnixNow
        {
            get
            {
                return Convert.ToInt64((Now - Unix).TotalMilliseconds);
            }
        }

        public static long UnixUtcNow
        {
            get
            {
                return Convert.ToInt64((UtcNow - Unix).TotalMilliseconds);
            }
        }

        static Time()
        {
            Unix = new DateTime(1970, 1, 1);

            try
            {
                long filetime;
                GetSystemTimePreciseAsFileTime(out filetime);
                IsAvailable = true;
            }
            catch (EntryPointNotFoundException)
            {
                // Not running Windows 8 or higher. 
                Logger.Error(LanguageFile.Exception_HighResTimeNotAvailable);
                IsAvailable = false;
            }
        }
    }
}

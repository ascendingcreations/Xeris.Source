﻿using System.Collections.Generic;
using System.Linq;

namespace Xeris.Library.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> Invert<T>(this IEnumerable<T> source)
        {
            var transform = source.Select(
                (o, i) => new
                {
                    Index = i,
                    Object = o
                });

            return transform.OrderByDescending(o => o.Index)
                            .Select(o => o.Object);
        }
    }
}

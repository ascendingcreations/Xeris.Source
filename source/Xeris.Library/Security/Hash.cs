﻿using System;
using System.Collections.Concurrent;
using System.Security.Cryptography;

namespace Xeris.Library.Security
{
    public static class Hash
    {
        private static ConcurrentDictionary<string, HashType> hashtypes;

        public static HashType SHA256 { get { return Get<SHA256>(); } }
        public static HashType SHA384 { get { return Get<SHA384>(); } }
        public static HashType SHA512 { get { return Get<SHA512>(); } }
        public static HashType RIPEMD160 { get { return Get<RIPEMD160>(); } }

        static Hash()
        {
            hashtypes = new ConcurrentDictionary<string, HashType>();
            
            Register<SHA256>(System.Security.Cryptography.SHA256.Create());
            Register<SHA384>(System.Security.Cryptography.SHA384.Create());
            Register<SHA512>(System.Security.Cryptography.SHA512.Create());
            Register<RIPEMD160>(System.Security.Cryptography.RIPEMD160.Create());
        }

        public static HashType Get<T>()
        {
            var sanitizedName = new String(typeof(T).Name.ToCharArray()).ToLower();

            if (hashtypes.ContainsKey(sanitizedName))
            {
                return hashtypes[sanitizedName];
            }
            
            return null;
        }

        public static bool Register<T>(HashAlgorithm algorithm)
        {
            var sanitizedName = new String(typeof(T).Name.ToCharArray()).ToLower();

            if (!hashtypes.ContainsKey(sanitizedName))
            {
                if (algorithm != null)
                {
                    hashtypes[sanitizedName] = new HashType(algorithm);
                    return true;
                }
            }

            return false;
        }
    }
}

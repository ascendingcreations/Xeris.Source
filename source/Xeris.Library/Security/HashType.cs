﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Xeris.Library.Security
{
    public class HashType : IDisposable
    {
        private HashAlgorithm algorithm;

        public HashType(HashAlgorithm algorithm)
        {
            this.algorithm = algorithm;
        }

        public void Dispose()
        {
            this.algorithm.Dispose();
            this.algorithm = null;
        }

        public uint Length
        {
            get
            {
                return Convert.ToUInt32(this.Encode("randomstringtocheckthelength").Length);
            }
        }

        public byte[] Encode(byte[] data)
        {
            // step 1, calculate hash from input
            return this.algorithm.ComputeHash(data);
        }

        public string Encode(string data)
        {
            // step 1, calculate hash from input
            byte[] dataBytes = Encoding.UTF8.GetBytes(data);
            byte[] hash = this.algorithm.ComputeHash(dataBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public byte[] Decode(byte[] data)
        {
            throw new NotImplementedException();
        }

        public string Decode(string data)
        {
            throw new NotImplementedException();
        }

        public bool Compare(byte[] data, byte[] hash)
        {
            // step 1, calculate hash from input
            byte[] hash2 = this.Encode(data);

            // step 2, compare hashes.
            return (hash2 == hash);
        }

        public bool Compare(string data, string hash)
        {
            // step 1, calculate hash from input
            string hash2 = this.Encode(data);

            // step 2, compare hashes.
            return (hash2 == hash);
        }
    }
}

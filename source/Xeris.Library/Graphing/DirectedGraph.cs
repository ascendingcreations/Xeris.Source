﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xeris.Library.Logging;
using Xeris.Language;

namespace Xeris.Library.Graphing
{
    public class DirectedGraph<T> : IEnumerator<T>
    {
        private Dictionary<T, SortedSet<T>> graph;
        private List<T> orderedNodes;
        int position;
        private Type myType;

        public Dictionary<T, SortedSet<T>> Graph { get { return graph; } }
        public List<T> OrderedNodes { get { return orderedNodes; } }

        public DirectedGraph()
        {
            graph = new Dictionary<T, SortedSet<T>>();
            orderedNodes = new List<T>();
            position = -1;
            myType = typeof(T);
        }

        public T Current
        {
            get
            {
                try
                {
                    return graph.Keys.ElementAt(position);
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public int Size
        {
            get { return graph.Count; }
        }

        public bool IsEmpty()
        {
            return (Size <= 0);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool MoveNext()
        {
            position++;
            return (position < Size);
        }

        public void Reset()
        {
            position = -1;
        }

        public override string ToString()
        {
            return myType.ToString();
        }

        public bool AddNode(T node)
        {
            if (graph.ContainsKey(node))
            {
                Logger.Error(LanguageFile.Graph_AddNode_Fail, this.ToString(), node.ToString());
                return false;
            }

            orderedNodes.Add(node);

            using (DirectedGraphComparator<T> comparator = new DirectedGraphComparator<T>(this))
                graph.Add(node, new SortedSet<T>(comparator));

            Logger.Debug(LanguageFile.Graph_AddNode_Success, this.ToString(), node.ToString());
            return true;
        }

        public void AddEdge(T from, T to)
        {
            if (!graph.ContainsKey(from))
                throw new Exception(string.Format(LanguageFile.Graph_NodeMissing, this.ToString(), from.ToString()));

            if (!graph.ContainsKey(to))
                throw new Exception(string.Format(LanguageFile.Graph_NodeMissing, this.ToString(), to.ToString()));

            if (!EdgeExists(from, to))
                if (graph[from].Add(to))
                    Logger.Debug(string.Format(LanguageFile.Graph_AddEdge_Success, this.ToString(), from.ToString(), to.ToString()));
                else
                    Logger.Error(string.Format(LanguageFile.Graph_AddEdge_Fail, this.ToString(), from.ToString(), to.ToString()));
        }

        public void RemoveEdge(T from, T to)
        {
            if (!graph.ContainsKey(from))
                throw new Exception(string.Format(LanguageFile.Graph_NodeMissing, this.ToString(), from.ToString()));

            if (!graph.ContainsKey(to))
                throw new Exception(string.Format(LanguageFile.Graph_NodeMissing, this.ToString(), to.ToString()));

            if (EdgeExists(from, to))
                if (graph[from].Remove(to))
                    Logger.Debug(string.Format(LanguageFile.Graph_RemoveEdge_Success, this.ToString(), from.ToString(), to.ToString()));
                else
                    Logger.Error(string.Format(LanguageFile.Graph_RemoveEdge_Fail, this.ToString(), from.ToString(), to.ToString()));
        }

        public bool EdgeExists(T from, T to)
        {
            if (!(graph.ContainsKey(from) && graph.ContainsKey(to)))
                return false;

            return graph[from].Contains(to);
        }

        public SortedSet<T> EdgesFrom(T from)
        {
            if (!graph.ContainsKey(from))
                throw new Exception(string.Format(LanguageFile.Graph_NodeMissing, this.ToString(), from.ToString()));

            return graph[from];
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Xeris.Library.Exceptions;
using Xeris.Library.Logging;

namespace Xeris.Library.Graphing
{
    public static class TopologicalSort
    {
        public static List<T> Sort<T>(DirectedGraph<T> graph)
        {
            DirectedGraph<T> rGraph = Reverse(graph);
            List<T> sortedResult = new List<T>();
            HashSet<T> visitedNodes = new HashSet<T>();
            // A list of "fully explored" nodes. Leftovers in here indicate cycles in the graph
            HashSet<T> expandedNodes = new HashSet<T>();

            foreach(T node in graph.Graph.Keys)
            {
                Explore(node, rGraph, sortedResult, visitedNodes, expandedNodes);
            }

            return sortedResult;
        }

        public static DirectedGraph<T> Reverse<T>(DirectedGraph<T> graph)
        {
            DirectedGraph<T> result = new DirectedGraph<T>();

            foreach(T node in graph.Graph.Keys)
                result.AddNode(node);

            foreach(T from in graph.Graph.Keys)
                foreach (T to in graph.EdgesFrom(from))
                    result.AddEdge(to, from);

            return result;
        }

        public static void Explore<T>(T node, DirectedGraph<T> graph, List<T> sortedResult, HashSet<T> visitedNodes, HashSet<T> expandedNodes)
        {
            // Have we been here before?
            if (visitedNodes.Contains(node))
            {
                // And have completed this node before
                if (expandedNodes.Contains(node))
                {
                    // Then we're fine
                    return;
                }

                Logger.Error("Topological Sorting failed.");
                Logger.Error("Visting node {0}", node);
                Logger.Error("Current sorted list : {0}", sortedResult);
                Logger.Error("Visited set for this node : {0}", visitedNodes);
                Logger.Error("Explored node set : {0}", expandedNodes);

                List<T> cycleList = (List<T>)visitedNodes.Except<T>(expandedNodes);
                Logger.Error("Likely cycle is in : %s", cycleList);
                throw new TopologicalSortingException<T>(node, cycleList);
            }

            // Visit this node
            visitedNodes.Add(node);

            // Recursively explore inbound edges
            foreach (T inbound in graph.EdgesFrom(node))
            {
                Explore(inbound, graph, sortedResult, visitedNodes, expandedNodes);
            }

            // Add ourselves now
            sortedResult.Add(node);
            // And mark ourselves as explored
            expandedNodes.Add(node);
        }
    }
}

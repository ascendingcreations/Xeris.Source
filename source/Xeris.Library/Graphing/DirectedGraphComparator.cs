﻿using System;
using System.Collections.Generic;

namespace Xeris.Library.Graphing
{
    public class DirectedGraphComparator<T> : IComparer<T>, IDisposable
    {
        public DirectedGraph<T> graph;

        public DirectedGraphComparator(DirectedGraph<T> graph)
        {
            this.graph = graph;
        }

        ~DirectedGraphComparator()
        {
            this.graph = null;
        }

        public int Compare(T x, T y)
        {
            if (graph == null)
                throw new ObjectDisposedException("graph");

            return (graph.OrderedNodes.IndexOf(x) - graph.OrderedNodes.IndexOf(y));
        }

        public void Dispose()
        {
            // Not used, only created for IDisposable interface requirement.
        }
    }
}

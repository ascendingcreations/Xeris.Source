﻿using System.Collections.Generic;
using System.Linq;
using Xeris.Library.Graphing;

namespace Xeris.Plugins
{
    public class PluginSorter
    {
        private DirectedGraph<XerisPlugin> graph;

        private XerisPlugin beforeAll = new DummyPlugin("BeforeAll");
        private XerisPlugin afterAll = new DummyPlugin("AfterAll");
        private XerisPlugin before = new DummyPlugin("Before");
        private XerisPlugin after = new DummyPlugin("After");

        public PluginSorter(List<XerisPlugin> plugins)
        {
            buildGraph(plugins);
        }

        private void buildGraph(List<XerisPlugin> plugins)
        {
            graph = new DirectedGraph<XerisPlugin>();
            graph.AddNode(beforeAll);
            graph.AddNode(before);
            graph.AddNode(afterAll);
            graph.AddNode(after);
            graph.AddEdge(before, after);
            graph.AddEdge(beforeAll, before);
            graph.AddEdge(after, afterAll);

            foreach (XerisPlugin m in plugins)
            {
                graph.AddNode(m);
            }

            foreach (XerisPlugin m in plugins)
            {
                if (m.IsImmutable)
                {
                    // Immutable mods are always before everything
                    graph.AddEdge(beforeAll, m);
                    graph.AddEdge(m, before);
                    continue;
                }
                bool preDepAdded = false;
                bool postDepAdded = false;

                foreach (XerisPlugin dep in m.Dependencies)
                {
                    preDepAdded = true;

                    if (dep.Name.Equals("*"))
                    {
                        // We are "after" everything
                        graph.AddEdge(m, afterAll);
                        graph.AddEdge(after, m);
                        postDepAdded = true;
                    }
                    else
                    {
                        graph.AddEdge(before, m);
                        if (plugins.Contains<XerisPlugin>(dep))
                            graph.AddEdge(dep, m);
                    }
                }

                foreach (XerisPlugin dep in m.Dependents)
                {
                    if (dep == null) { continue; }

                    postDepAdded = true;

                    if (dep.Name.Equals("*"))
                    {
                        // We are "before" everything
                        graph.AddEdge(beforeAll, m);
                        graph.AddEdge(m, before);
                        preDepAdded = true;
                    }
                    else
                    {
                        graph.AddEdge(m, after);
                        if (plugins.Contains<XerisPlugin>(dep))
                            graph.AddEdge(m, dep);
                    }
                }

                if (!preDepAdded)
                    graph.AddEdge(before, m);

                if (!postDepAdded)
                    graph.AddEdge(m, after);
            }
        }

        public List<XerisPlugin> Sort()
        {
            List<XerisPlugin> sortedList = TopologicalSort.Sort(graph);

            sortedList.Remove(before);
            sortedList.Remove(beforeAll);
            sortedList.Remove(after);
            sortedList.Remove(afterAll);

            return sortedList;
        }
    }
}

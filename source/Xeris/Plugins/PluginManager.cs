﻿using System;
using System.Collections.Generic;

namespace Xeris.Plugins
{
    public class PluginManager : IDisposable
    {
        private XerisApplication parent;

        private List<XerisPlugin> plugins;
        public List<XerisPlugin> Plugins { get { return plugins; } }

        public PluginManager(XerisApplication parent)
        {
            this.parent = parent;
            this.plugins = new List<XerisPlugin>();
        }

        public void Dispose()
        {

        }
    }
}

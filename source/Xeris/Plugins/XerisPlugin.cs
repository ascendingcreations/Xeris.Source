﻿using System;

namespace Xeris.Plugins
{
    public class XerisPlugin : IDisposable
    {
        protected XerisApplication parent { get; private set; }

        public virtual string Name { get { return this.GetType().Name; } }
        public virtual Version Version { get { return new Version("1.0.0"); } }
        public virtual XerisPlugin[] Dependents { get { return new XerisPlugin[] { }; } }
        public virtual XerisPlugin[] Dependencies { get { return new XerisPlugin[] { }; } }
        public virtual bool IsImmutable { get { return false; } }

        public bool Disposed { get; private set; }

        public XerisPlugin(XerisApplication parent)
        {
            this.parent = parent;
        }

        public void Dispose()
        {
            if (this.Disposed) { return; }

            this.parent = null;
            this.Disposed = true;
        }

        public virtual void OnPluginInit() { }
        public virtual void OnPluginDispose() { }
    }
}

﻿namespace Xeris.Plugins
{
    public class DummyPlugin : XerisPlugin
    {
        private string name;
        public override string Name { get { return this.name; } }

        public DummyPlugin(string name) : base(null)
        {
            this.name = name;
        }
    }
}

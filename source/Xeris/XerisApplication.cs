﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Xeris.Modules;
using Xeris.Plugins;
using Xeris.Library.Extensions;
using Xeris.Library.Logging;

namespace Xeris
{
    public abstract class XerisApplication : IDisposable
    {
        private static XerisApplication instance;
        public static XerisApplication Instance
        {
            get { return instance; }
            set
            {
                if(instance != null)
                {
                    if (instance.isRunning)
                    {
                        Logger.Log("A new application is attempting to start, terminating " + instance.GetType().Name + "...");

                        instance.Stop();

                        while (instance.isRunning) { }
                    }
                }

                Logger.Log("Initializing application " + value.GetType().Name + "...");
                instance = value;
            }
        }

        public ModuleManager Module { get; private set; }
        public PluginManager Plugin { get; private set; }
        private bool disposedValue = false;
        private bool isRunning = false;
        private bool doLoop = false;

        public List<XerisModule> Modules { get { return Module.Modules; } }
        public List<XerisPlugin> Plugins { get { return Plugin.Plugins; } }

        public XerisApplication()
        {
            this.Plugin = new PluginManager(this);

            this.Module = new ModuleManager(this);
            this.RegisterModules();
        }

        public void RunSingle()
        {
            this.Run(true);
        }

        public void Run()
        {
            this.Run(false);
        }

        private void Run(bool single)
        {
            if (this.isRunning) { return; }

            this.isRunning = true;

            foreach (XerisPlugin plugin in this.Plugins)
                plugin.OnPluginInit();

            Module.Update();

            if (this.Modules.Count() == 0) { return; }

            Thread app = new Thread(() => {
                this.doLoop = (!single);

                foreach (XerisModule module in this.Modules)
                    module.OnModuleInit();

                foreach (XerisModule module in this.Modules)
                    module.OnEngineInit();

                while (this.doLoop)
                {
                    foreach (XerisModule module in this.Modules)
                        module.OnBeforeUpdate();

                    foreach (XerisModule module in this.Modules)
                        module.OnUpdate();

                    foreach (XerisModule module in this.Modules.Invert())
                        module.OnAfterUpdate();
                }

                foreach (XerisModule module in this.Modules.Invert())
                    module.OnEngineDispose();

                foreach (XerisModule module in this.Modules.Invert())
                    module.OnModuleDispose();
            });
            app.Start();

            while (app.IsAlive)
            {
                Application.DoEvents();
            }

            foreach (XerisPlugin plugin in this.Plugins)
                plugin.OnPluginDispose();

            this.isRunning = false;
        }

        public void Stop()
        {
            this.doLoop = false;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (this.Module != null)
                        this.Module.Dispose();

                    if (this.Plugin != null)
                        this.Plugin.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        protected virtual void RegisterModules()
        {

        }
    }
}

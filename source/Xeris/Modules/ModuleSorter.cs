﻿using System.Collections.Generic;
using System.Linq;
using Xeris.Library.Graphing;

namespace Xeris.Modules
{
    public class ModuleSorter
    {
        private DirectedGraph<XerisModule> graph;

        private XerisModule beforeAll = new DummyModule("BeforeAll");
        private XerisModule afterAll = new DummyModule("AfterAll");
        private XerisModule before = new DummyModule("Before");
        private XerisModule after = new DummyModule("After");

        public ModuleSorter(List<XerisModule> modules)
        {
            buildGraph(modules);
        }

        private void buildGraph(List<XerisModule> modules)
        {
            graph = new DirectedGraph<XerisModule>();
            graph.AddNode(beforeAll);
            graph.AddNode(before);
            graph.AddNode(afterAll);
            graph.AddNode(after);
            graph.AddEdge(before, after);
            graph.AddEdge(beforeAll, before);
            graph.AddEdge(after, afterAll);

            foreach(XerisModule m in modules)
            {
                graph.AddNode(m);
            }

            foreach (XerisModule m in modules)
            {
                if (m.IsImmutable)
                {
                    // Immutable mods are always before everything
                    graph.AddEdge(beforeAll, m);
                    graph.AddEdge(m, before);
                    continue;
                }
                bool preDepAdded = false;
                bool postDepAdded = false;

                foreach (XerisModule dep in m.Dependencies)
                {
                    preDepAdded = true;

                    if (dep.Name.Equals("*"))
                    {
                        // We are "after" everything
                        graph.AddEdge(m, afterAll);
                        graph.AddEdge(after, m);
                        postDepAdded = true;
                    }
                    else
                    {
                        graph.AddEdge(before, m);
                        if (modules.Contains<XerisModule>(dep))
                            graph.AddEdge(dep, m);
                    }
                }

                foreach (XerisModule dep in m.Dependents)
                {
                    if (dep == null) { continue; }

                    postDepAdded = true;

                    if (dep.Name.Equals("*"))
                    {
                        // We are "before" everything
                        graph.AddEdge(beforeAll, m);
                        graph.AddEdge(m, before);
                        preDepAdded = true;
                    }
                    else
                    {
                        graph.AddEdge(m, after);
                        if (modules.Contains<XerisModule>(dep))
                            graph.AddEdge(m, dep);
                    }
                }

                if (!preDepAdded)
                    graph.AddEdge(before, m);

                if (!postDepAdded)
                    graph.AddEdge(m, after);
            }
        }

        public List<XerisModule> Sort()
        {
            List<XerisModule> sortedList = TopologicalSort.Sort(graph);

            sortedList.Remove(before);
            sortedList.Remove(beforeAll);
            sortedList.Remove(after);
            sortedList.Remove(afterAll);

            return sortedList;
        }
    }
}

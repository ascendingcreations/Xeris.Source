﻿namespace Xeris.Modules
{
    public class DummyModule : XerisModule
    {
        private string name;
        public override string Name { get { return this.name; } }

        public DummyModule(string name) : base(null)
        {
            this.name = name;
        }

        public override string ToString()
        {
            return base.ToString().Replace("DummyModule", this.name);
        }
    }
}

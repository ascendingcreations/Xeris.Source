﻿using System;

namespace Xeris.Modules
{
    public abstract class XerisModule : IDisposable
    {
        protected XerisApplication parent { get; private set; }

        public virtual string Name { get { return this.GetType().Name; } }
        public virtual XerisModule[] Dependents { get { return new XerisModule[] { }; } }
        public virtual XerisModule[] Dependencies { get { return new XerisModule[] { }; } }
        public virtual bool IsImmutable { get { return false; } }

        public bool Disposed { get; private set; }

        public XerisModule(XerisApplication parent)
        {
            this.parent = parent;
        }

        public void Dispose()
        {
            if (this.Disposed) { return; }
            
            this.parent = null;
            this.Disposed = true;
        }

        public virtual void OnModuleInit()
        {
            if (this.Dependencies != null) {
                foreach(XerisModule t in this.Dependencies) {
                    parent.Module.DependencyCheck(GetType(), t.GetType());
                }
            }
        }
        public virtual void OnModuleDispose() { }
        public virtual void OnEngineInit() { }
        public virtual void OnEngineDispose() { }
        public virtual void OnBeforeUpdate() { }
        public virtual void OnUpdate() { }
        public virtual void OnAfterUpdate() { }
    }
}

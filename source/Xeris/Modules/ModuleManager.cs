﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Xeris.Exceptions;

namespace Xeris.Modules
{
    public class ModuleManager : IDisposable
    {
        private XerisApplication parent;
        private ConcurrentDictionary<Type, XerisModule> umodules;

        private List<XerisModule> modules;
        public List<XerisModule> Modules { get { return modules; } }

        public ModuleManager(XerisApplication parent)
        {
            this.parent = parent;
            this.umodules = new ConcurrentDictionary<Type, XerisModule>();
        }

        public void Dispose()
        {
            if (this.modules != null)
            {
                foreach (XerisModule module in this.modules)
                    module.Dispose();

                this.modules.Clear();
                this.modules = null;
            }

            if(this.umodules != null)
            {
                this.umodules.Clear();
                this.umodules = null;
            }
        }

        public T Add<T>(T module = null, params object[] args) where T : XerisModule
        {
            if (!typeof(XerisModule).IsAssignableFrom(typeof(T)))
                return null;

            if (umodules.ContainsKey(typeof(T)))
                return null;

            if (args == null || args.Length < 1 || args[0] != parent)
            {
                object[] newArgs = new object[args.Length + 1];
                newArgs[0] = parent;
                Array.Copy(args, 0, newArgs, 1, args.Length);
                args = newArgs;
            }

            var instance = module;
            if (instance == null)
                instance = (T)Activator.CreateInstance(typeof(T), args);

            if (umodules.TryAdd(typeof(T), instance))
                return instance;

            return null;
        }

        public void DependencyCheck(Type dependent, Type dependency)
        {
            if (!this.IsLoaded(dependency))
                throw new DependencyException(dependent, dependency);
        }

        public T Get<T>() where T : XerisModule
        {
            var result = (XerisModule)null;
            if (umodules.TryGetValue(typeof(T), out result))
                return (T)result;
            return null;
        }

        public bool IsLoaded<T>() where T : XerisModule
        {
            var result = (XerisModule)null;
            if (umodules.TryGetValue(typeof(T), out result))
                return true;
            return false;
        }

        public bool IsLoaded(Type t)
        {
            var result = (XerisModule)null;
            if (umodules.TryGetValue(t, out result))
                return true;
            return false;
        }

        public bool Remove<T>() where T : XerisModule
        {
            var module = (T)null;
            return Remove<T>(out module);
        }

        public bool Remove<T>(out T module) where T : XerisModule
        {
            var instance = (XerisModule)null;
            var result = umodules.TryRemove(typeof(T), out instance);
            module = (T)instance;
            return result;
        }

        public void Update()
        {
            ModuleSorter ms = new ModuleSorter(umodules.Values.ToList());
            List<XerisModule> sorted = ms.Sort();

            if (modules != null)
            {
                modules.Clear();
                modules = null;
            }

            modules = new List<XerisModule>();

            foreach (XerisModule module in sorted)
                modules.Add(module);
        }
    }
}

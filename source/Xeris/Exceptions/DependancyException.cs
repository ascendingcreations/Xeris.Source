﻿using System;
using Xeris.Language;

namespace Xeris.Exceptions
{
    public class DependencyException : Exception
    {
        private Type dependent;
        private Type dependency;

        public DependencyException(Type dependent, Type dependency)
        {
            this.dependent = dependent;
            this.dependency = dependency;
        }

        ~DependencyException()
        {
            this.dependent = null;
            this.dependency = null;
        }

        public override string Message
        {
            get
            {
                return string.Format(LanguageFile.Exception_DependencyException, dependency.Name, dependent.Name);
            }
        }
    }
}

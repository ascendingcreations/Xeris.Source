﻿using Xeris.Client.Modules;

namespace Xeris.Client
{
    public class XerisClient : XerisApplication
    {
        protected override void RegisterModules()
        {
            base.RegisterModules();

            this.Module.Add<SfmlDebugFontModule>();
            this.Module.Add<SfmlDebugModule>();
            this.Module.Add<ImmutableDebugModule>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Xeris.Library.Logging;
using Xeris.Library.Utilities;
using Xeris.Modules;

namespace Xeris.Client
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Logger.Attach("logs\\" + Time.UnixNow + ".txt");

            using (XerisApplication.Instance = new XerisClient())
            {
                using (XerisApplication.Instance = new XerisClient())
                {
                    using (XerisApplication.Instance = new XerisClient())
                    {
                        XerisApplication.Instance.RunSingle();
                    }
                }
            }
        }
    }
}

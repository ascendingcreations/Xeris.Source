﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xeris.Modules;

namespace Xeris.Client.Modules
{
    public class ImmutableDebugModule : XerisModule
    {
        public ImmutableDebugModule(XerisApplication parent) : base(parent)
        {

        }

        public override bool IsImmutable
        {
            get
            {
                return true;
            }
        }
    }
}

﻿using SFML.Graphics;
using SFML.System;
using System;
using System.IO;
using Xeris.Modules;

namespace Xeris.Client.Modules
{
    public class SfmlDebugFontModule : XerisModule
    {
        private Font fnt;
        private Text txt;

        private bool testVar;

        public SfmlDebugFontModule(XerisApplication parent) : base(parent)
        {

        }

        public override XerisModule[] Dependencies
        {
            get
            {
                return new XerisModule[] {
                    parent.Module.Get<SfmlDebugModule>()
                };
            }
        }

        public override void OnEngineInit()
        {
            base.OnEngineInit();

            this.fnt = new Font(Environment.GetFolderPath(Environment.SpecialFolder.Fonts) + Path.DirectorySeparatorChar + "georgia.ttf");
            this.txt = new Text(string.Empty, fnt, 24);
        }

        public override void OnUpdate()
        {
            base.OnUpdate();

            DrawText("Some profound text,", 104, 104, Color.Yellow);
            DrawText("will be drawn here!", 132, 132, Color.Magenta);
            DrawText("Thread lock test: " + this.testVar, 160, 160, Color.White);
        }

        public override void OnAfterUpdate()
        {
            base.OnAfterUpdate();

            this.testVar = !this.testVar;
        }

        public override void OnEngineDispose()
        {
            base.OnEngineDispose();

            this.txt.Dispose();
            this.txt = null;

            this.fnt.Dispose();
            this.fnt = null;
        }

        private void DrawText(string text, int x, int y, Color color)
        {
            txt.DisplayedString = text;

            txt.Color = Color.Black;
            txt.Position = new Vector2f(x+1, y+1);
            txt.Draw(parent.Module.Get<SfmlDebugModule>().Window, RenderStates.Default);

            txt.Color = color;
            txt.Position = new Vector2f(x, y);
            txt.Draw(parent.Module.Get<SfmlDebugModule>().Window, RenderStates.Default);
        }
    }
}

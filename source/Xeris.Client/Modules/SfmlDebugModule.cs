﻿using System;
using SFML.Window;
using SFML.Graphics;
using Xeris.Modules;

namespace Xeris.Client.Modules
{
    public class SfmlDebugModule : XerisModule
    {
        private RenderWindow window;

        public RenderWindow Window { get { return this.window; } }

        public SfmlDebugModule(XerisApplication parent) : base(parent)
        {
            
        }

        public override XerisModule[] Dependents
        {
            get
            {
                return new XerisModule[] {
                    parent.Module.Get<SfmlDebugFontModule>()
                };
            }
        }

        public override void OnEngineInit()
        {
            base.OnEngineInit();

            window = new RenderWindow(new VideoMode(1024, 576), "Xeris.Source Client");
            window.Closed += Window_Closed;
        }

        public override void OnEngineDispose()
        {
            base.OnEngineDispose();

            window.Close();
            window = null;
        }

        public override void OnBeforeUpdate()
        {
            window.Clear(Color.Cyan);
        }

        public override void OnAfterUpdate()
        {
            window.Display();
            window.DispatchEvents();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            parent.Stop();
        }
    }
}
